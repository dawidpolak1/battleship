package board;

public class InvalidCoordinateException extends Exception {
    String coordinate;

    public InvalidCoordinateException(String coordinate) {
        this.coordinate = coordinate;
    }

    public String toString() {
        return "Invalid coordinate " + coordinate;
    }
}
