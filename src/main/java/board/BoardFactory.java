package board;

import java.util.ArrayList;
import java.util.List;

import static board.Graphic.*;

public abstract class BoardFactory {
    char[][] boardCoordinates;

    public BoardFactory() {
        this.boardCoordinates = new char[10][10];
        boardCoordinates = fillBoardWithWater();
    }

    public Board getBoard() {
        return new Board(boardCoordinates);
    }

    void placeShipOnBoard(int[] shipCoordinates) {
        for (int i = 0; i < shipCoordinates.length; i = i + 2) {
            boardCoordinates[shipCoordinates[i]][shipCoordinates[i + 1]] = SHIP;
        }
    }

    static char[][] fillBoardWithWater() {
        char[][] coordinates = new char[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                coordinates[i][j] = WATER;
            }
        }
        return coordinates;
    }

    boolean shipPlacementCorrect(int[] shipPlacement) {
        List<Boolean> eachShipCoordinateIsCorrect = new ArrayList<>();
        for (int i = 0; i < shipPlacement.length; i = i + 2) {
            eachShipCoordinateIsCorrect.add(canBePlacedOnBoard(shipPlacement[i], shipPlacement[i + 1]));
        }
        return !eachShipCoordinateIsCorrect.contains(false);
    }


    private boolean canBePlacedOnBoard(int row, int column) {
        if (isOutsideOfBoard(row, column)) {
            return false;
        }
        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = column - 1; j <= column + 1; j++) {
                try {
                    if (boardCoordinates[i][j] != WATER) {
                        return false;
                    }
                } catch (ArrayIndexOutOfBoundsException exc) {
                    //happens when neighbour coordinates is outside of board, don't have to do anything with it - it is still a valid coordinate.
                }
            }
        }
        return true;
    }

    private static boolean isOutsideOfBoard(int i, int j) {
        return i < 0 || i > 9 || j < 0 || j > 9;
    }
}