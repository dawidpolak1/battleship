package board;

import static board.Graphic.COLUMNS;
import static board.Graphic.ROWS;

public class CoordinatesTools {
    private static final String POINT_REGEX = "[A-J]([1-9]|10)";
    private static final String HORIZONTAL_LINE_REGEX = "([A-J])([1-9]|10)-(\\1)([1-9]|10)";
    private static final String VERTICAL_LINE_REGEX = "([A-J])([1-9]|10)-([A-J])(\\2)";

    static int[] transformCoordinates(String coordinates) throws InvalidCoordinateException {
        int[] coordinatesIndex;
        if (isHorizontal(coordinates)) {
            coordinatesIndex = getHorizontalCoordinate(coordinates);
        } else if (isVertical(coordinates)) {
            coordinatesIndex = getVerticalCoordinate(coordinates);
        } else if (isSinglePoint(coordinates)) {
            coordinatesIndex = getPointCoordinate(coordinates);
        } else throw new InvalidCoordinateException(coordinates);
        return coordinatesIndex;
    }

    static int getShipSize(String coordinate) throws InvalidCoordinateException {
        int size;
        if (isHorizontal(coordinate)) {
            size = getHorizontalSize(coordinate);
        } else if (isVertical(coordinate)) {
            size = getVerticalSize(coordinate);
        } else if (isSinglePoint(coordinate)) {
            size = 1;
        } else throw new InvalidCoordinateException(coordinate);
        return size;
    }

    static boolean isValidCoordinate(String coordinate) {
        return isSinglePoint(coordinate) || isHorizontal(coordinate) || isVertical(coordinate);
    }

    static boolean isValidCoordinate(int[] coordinate) {
        boolean correctRow = coordinate[0]>=0 && coordinate[0]<10;
        boolean correctColumn = coordinate[1]>=0 && coordinate[1]<10;
        return correctRow && correctColumn;
    }

    private static boolean isHorizontal(String coordinate) {
        return coordinate.matches(HORIZONTAL_LINE_REGEX);
    }

    private static boolean isVertical(String coordinate) {
        return coordinate.matches(VERTICAL_LINE_REGEX);
    }

    private static boolean isSinglePoint(String coordinate) {
        return coordinate.matches(POINT_REGEX);
    }

    private static int getHorizontalSize(String coordinate) {
        int pauseIndex = coordinate.indexOf('-');
        int startColumn = Integer.parseInt((coordinate.substring(1, pauseIndex)));
        int endColumn = Integer.parseInt((coordinate.substring(pauseIndex + 2)));
        return 1 + Math.abs(endColumn - startColumn);
    }

    private static int getVerticalSize(String coordinate) {
        int pauseIndex = coordinate.indexOf('-');
        char startRow = coordinate.charAt(0);
        char endRow = coordinate.charAt(pauseIndex + 1);
        return 1 + Math.abs(transformRow(endRow) - transformRow(startRow));
    }

    private static int[] getHorizontalCoordinate(String coordinate) throws InvalidCoordinateException {
        char row = coordinate.charAt(0);
        int lowerColumn = getLowerColumn(coordinate);
        int size = getShipSize(coordinate);
        int[] coordinatesIndex = new int[size * 2];
        for (int i = 0; i < size * 2; i++) {
            if (i % 2 == 0) {
                coordinatesIndex[i] = transformRow(row);
            } else {
                coordinatesIndex[i] = lowerColumn + i / 2;
            }
        }
        return coordinatesIndex;
    }

    private static int[] getVerticalCoordinate(String coordinate) throws InvalidCoordinateException {
        int pauseIndex = coordinate.indexOf('-');
        int column = Integer.parseInt((coordinate.substring(1, pauseIndex)));
        int lowerRow = getLowerRow(coordinate);
        int size = getShipSize(coordinate);
        int[] coordinatesIndex = new int[size * 2];
        for (int i = 0; i < size * 2; i++) {
            if (i % 2 == 0) {
                coordinatesIndex[i] = lowerRow + i / 2;
            } else {
                coordinatesIndex[i] = transformColumn(column);
            }
        }
        return coordinatesIndex;
    }

    private static int[] getPointCoordinate(String coordinate) {
        char row = coordinate.charAt(0);
        int column = Integer.parseInt((coordinate.substring(1)));
        return new int[]{transformRow(row), transformColumn(column)};
    }

    private static int getLowerRow(String coordinate) {
        int pauseIndex = coordinate.indexOf('-');
        char startRow = coordinate.charAt(0);
        char endRow = coordinate.charAt(pauseIndex + 1);
        return Math.min(transformRow(startRow), transformRow(endRow));
    }

    private static int getLowerColumn(String coordinate) {
        int pauseIndex = coordinate.indexOf('-');
        int startColumn = Integer.parseInt((coordinate.substring(1, pauseIndex)));
        int endColumn = Integer.parseInt((coordinate.substring(pauseIndex + 2)));
        return Math.min(transformColumn(startColumn), transformColumn(endColumn));
    }

    private static int transformRow(char row) {
        int rowIndex = -1;
        for (int i = 0; i < ROWS.length; i++) {
            if (ROWS[i] == row) {
                rowIndex = i;
            }
        }
        return rowIndex;
    }

    private static int transformColumn(int column) {
        int columnIndex = -1;
        for (int i = 0; i < ROWS.length; i++) {
            if (COLUMNS[i] == column) {
                columnIndex = i;
            }
        }
        return columnIndex;
    }
}