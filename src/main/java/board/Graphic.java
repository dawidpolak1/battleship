package board;

public class Graphic {
    static final int[] COLUMNS = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    static final char[] ROWS = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
    static final char WATER = '~';
    static final char SHIP = 'O';
    static final char HIT = 'X';
    static final char DISTANCE = ' ';
    static final String BOARDS_DISTANCE = "\t\t\t";
}
