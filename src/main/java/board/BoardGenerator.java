package board;

import java.util.Random;

public class BoardGenerator extends BoardFactory {
    private final Random random = new Random();

    public BoardGenerator() {
        super();
    }

    @Override
    public Board getBoard() {
        placeAllShips();
        return new Board(boardCoordinates);

    }

    private void placeAllShips() {
        placeSingleShipRandomly(4);

        placeSingleShipRandomly(3);
        placeSingleShipRandomly(3);

        placeSingleShipRandomly(2);
        placeSingleShipRandomly(2);
        placeSingleShipRandomly(2);

        placeSingleShipRandomly(1);
        placeSingleShipRandomly(1);
        placeSingleShipRandomly(1);
        placeSingleShipRandomly(1);
    }

    private void placeSingleShipRandomly(int shipSize) {
        boolean placementCorrect;
        int[] shipCoordinates;

        do {
            shipCoordinates = generateShipCoordinates(shipSize);
            placementCorrect = shipPlacementCorrect(shipCoordinates);
        } while (!placementCorrect);

        placeShipOnBoard(shipCoordinates);
    }

    private int[] generateShipCoordinates(int shipSize) {
        int[] shipCoordinates = new int[shipSize * 2];

        int currentRow = random.nextInt(10);
        int currentColumn = random.nextInt(10);
        Direction direction = getRandomDirection();

        for (int i = 0; i < shipCoordinates.length; i = i + 2) {
            shipCoordinates[i] = currentRow;
            shipCoordinates[i + 1] = currentColumn;
            currentRow = changeRowInSetDirection(currentRow, direction);
            currentColumn = changeColumnInSetDirection(currentColumn, direction);
        }

        return shipCoordinates;
    }

    private Direction getRandomDirection() {
        int seed = random.nextInt(4);
        Direction direction = switch (seed) {
            case 0 -> Direction.UP;
            case 1 -> Direction.RIGHT;
            case 2 -> Direction.DOWN;
            case 3 -> Direction.LEFT;
            default -> null;
        };
        return direction;
    }

    private int changeColumnInSetDirection(int column, Direction direction) {
        switch (direction) {
            case RIGHT -> column++;
            case LEFT -> column--;
        }
        return column;
    }

    private int changeRowInSetDirection(int row, Direction direction) {
        switch (direction) {
            case UP -> row--;
            case DOWN -> row++;
        }
        return row;
    }
}
