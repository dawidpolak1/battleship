package board;

import java.util.ArrayList;
import java.util.List;
import static board.Graphic.*;

public class BoardPrinter {

    public static void printBoards(char[][] coordinates) {
        printBoards(new Board(coordinates));
    }

    public static void printBoards(Board board) {
        List<Board> boards = new ArrayList<>();
        boards.add(board);
        printManyBoards(boards);
    }

    public static void printBoards(Board board1, Board board2) {
        List<Board> boards = new ArrayList<>();
        boards.add(board1);
        boards.add(board2);
        printManyBoards(boards);
    }

    public static void printBoards(Board board1, Board board2, Board board3, Board board4) {
        List<Board> boards = new ArrayList<>();
        boards.add(board1);
        boards.add(board2);
        boards.add(board3);
        boards.add(board4);
        printManyBoards(boards);
    }

    private static void printManyBoards(List<Board> boards) {
        printNamesLine(boards);
        printTopLine(boards);
        printBoardLines(boards);
    }

    private static void printNamesLine(List<Board> boards) {
        for (Board board : boards) {
            if (board.nameIsSet()) {
                printBoardName(board.getName());
            } else {
                printEmptyNameLine();
            }
            System.out.print(BOARDS_DISTANCE);
        }
        System.out.println();
    }

    private static void printTopLine(List<Board> boards) {
        for (int i = 0; i < boards.size(); i++) {
            printTopLine();
            System.out.print(BOARDS_DISTANCE);
        }
        System.out.println();
    }

    private static void printBoardLines(List<Board> boards) {
        for (int i = 0; i < 10; i++) {
            for (Board board : boards) {
                System.out.print(ROWS[i]);
                for (int j = 0; j < 10; j++) {
                    System.out.print(DISTANCE);
                    System.out.print(board.getBoardCoordinates()[i][j]);
                }
                System.out.print(BOARDS_DISTANCE);
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void printEmptyNameLine() {
        System.out.print("                     ");
    }

    private static void printBoardName(String boardName) {
        System.out.print(centerName(boardName));
    }

    private static String centerName(String name) {
        int boardWidth = 22;
        int sideDistanceLength = (boardWidth - name.length()) / 2;
        if (sideDistanceLength > 0) {
            return String.format("%2$" + sideDistanceLength + "s%1$s%2$" + sideDistanceLength + "s", name, " ");
        } else return name;
    }

    private static void printTopLine() {
        System.out.print(DISTANCE);
        for (int i = 0; i < 10; i++) {
            System.out.print(DISTANCE);
            System.out.print(COLUMNS[i]);
        }
    }
}