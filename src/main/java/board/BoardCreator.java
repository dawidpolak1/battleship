package board;

import java.util.ArrayList;
import java.util.List;

import static board.CoordinatesTools.getShipSize;
import static board.CoordinatesTools.transformCoordinates;

public class BoardCreator extends BoardFactory {
    boolean isComplete = false;
    List<String> shipList = new ArrayList<>();
    List<Integer> shipLengthList = new ArrayList<>();

    public BoardCreator() {
        super();
    }

    @Override
    public Board getBoard() {
        if (isComplete) {
            return new Board(boardCoordinates);
        } else {
            System.out.println("Board not complete.");
            return null;
        }
    }


    public void print() {
        BoardPrinter.printBoards(new Board(boardCoordinates));
    }


    public Board getEmptyBoard() {
        return new Board();
    }

    public boolean placeShip(String shipCoordinates) throws InvalidCoordinateException {
        int[] transformedShipCoordinates = transformCoordinates(shipCoordinates);
        boolean placementCorrect = shipPlacementCorrect(transformedShipCoordinates);
        int size = getShipSize(shipCoordinates);
        if (placementCorrect & canFillAnother(size)) {
            placeShipOnBoard(transformedShipCoordinates);
            manageShips(shipCoordinates);
            BoardPrinter.printBoards(boardCoordinates);
            return true;
        } else if (!placementCorrect) {
            System.out.println("Incorrect placement for coordinates " + shipCoordinates + ".");
            return false;
        } else if (!canFillAnother(size)) {
            if(size>4) {
                System.out.println("Incorrect ship size!");
            } else {
                System.out.println("Can't add more " + size + "-flag ships.");
            }
            return false;
        } else return false;
    }

    private void manageShips(String shipCoordinates) throws InvalidCoordinateException {
        shipList.add(shipCoordinates);
        shipLengthList.add(getShipSize(shipCoordinates));
        checkForCompletion();
    }

    private boolean canFillAnother(int shipSize) {
        switch (shipSize) {
            case 1 -> {
                return getOneFlagShipsAmount() < 4;
            }
            case 2 -> {
                return getTwoFlagShipsAmount() < 3;
            }
            case 3 -> {
                return getThreeFlagShipsAmount() < 2;
            }
            case 4 -> {
                return getFourFlagShipsAmount() < 1;
            }
            default -> {
                return false;
            }
        }
    }

    private void checkForCompletion() {
        if (getFourFlagShipsAmount() == 1 && getThreeFlagShipsAmount() == 2 && getTwoFlagShipsAmount() == 3 & getOneFlagShipsAmount() == 4) {
            isComplete = true;
        }
    }

    private int getFourFlagShipsAmount() {
        return (int) shipLengthList.stream().filter((size) -> size.equals(4)).count();
    }

    private int getThreeFlagShipsAmount() {
        return (int) shipLengthList.stream().filter((size) -> size.equals(3)).count();
    }

    private int getTwoFlagShipsAmount() {
        return (int) shipLengthList.stream().filter((size) -> size.equals(2)).count();
    }

    private int getOneFlagShipsAmount() {
        return (int) shipLengthList.stream().filter((size) -> size.equals(1)).count();
    }

    public boolean isComplete() {
        return isComplete;
    }
}
