package board;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static board.BoardFactory.fillBoardWithWater;
import static board.CoordinatesTools.isValidCoordinate;
import static board.CoordinatesTools.transformCoordinates;
import static board.Graphic.*;

public class Board {
    private char[][] boardCoordinates;
    private String boardName;
    //utility list for checking single ship coordinates, resets at the beginning of each check
    private List<int[]> wholeShip;

    //Returns empty board with name
    public Board(String boardName) {
        this.boardName = boardName;
        boardCoordinates = fillBoardWithWater();
    }

    //Returns empty board without name
    public Board() {
        boardCoordinates = fillBoardWithWater();
    }

    //Returns board with set coordinates without name
    public Board(char[][] coordinates) {
        this.boardCoordinates = coordinates;
    }

    //Returns board with set coordinates with name
    public Board(String boardName, char[][] coordinates) {
        this.boardName = boardName;
        this.boardCoordinates = coordinates;
    }

    public void print() {
        BoardPrinter.printBoards(this);
    }

    public char[][] getBoardCoordinates() {
        return boardCoordinates;
    }

    public String shoot(String shotCoordinate) throws InvalidCoordinateException {
        if (isValidCoordinate(shotCoordinate)) {
            int[] transformedShotCoordinate = transformCoordinates(shotCoordinate);
            return getShotResponse(transformedShotCoordinate);
        } else throw new InvalidCoordinateException(shotCoordinate);
    }

    public boolean allShipsSunken() {
        List<int[]> allShips = getShipsCoordinates();
        for(int[] shipField:allShips) {
            char coordinateContent = boardCoordinates[shipField[0]][shipField[1]];
            if(coordinateContent != HIT) {
                return false;
            }
        }
        return true;
    }

    private String getShotResponse(int[] shotCoordinate ) {
        int shotRow = shotCoordinate[0];
        int shotColumn = shotCoordinate[1];
        char coordinateContent = boardCoordinates[shotRow][shotColumn];
        boardCoordinates[shotRow][shotColumn] = HIT;
        return switch (coordinateContent) {
            case SHIP ->
                    //check ship size and return either hit or sink
                    analiseShip(shotCoordinate);
            case HIT -> "ALREADY SHOT AT";
            default -> "MISS";
        };
    }

    private String analiseShip(int[] shipCoordinate) {
        //resets wholeShip list
        wholeShip = new ArrayList<>();
        fillCurrentShipList(shipCoordinate);
        return shipSunken() ? "SINK" : "HIT";
    }

    private void fillCurrentShipList(int[] shotCoordinate) {
        wholeShip.add(shotCoordinate);
        List<int[]> fieldsToCheck = getSurroundingFields(shotCoordinate);
        for(int[] field:fieldsToCheck) {
            if(isValidCoordinate(field)) {
                char fieldContent = boardCoordinates[field[0]][field[1]];
                if(fieldContent!=WATER && !coordinateOnTheList(field)) {
                    //recursive call for another ship coordinate
                    fillCurrentShipList(field);
                }
            }
        }
    }

    private List<int[]> getSurroundingFields(int[] shotCoordinate) {
        int row = shotCoordinate[0];
        int column = shotCoordinate[1];
        return Arrays.asList(
                new int[]{row - 1 , column},
                new int[]{row, column + 1},
                new int[]{row + 1, column},
                new int[]{row, column - 1});
    }


    private boolean coordinateOnTheList(int[] coordinate) {
        for(int[] shipField:wholeShip) {
            if(shipField[0]==coordinate[0] && shipField[1]==coordinate[1]) {
                return true;
            }
        }
        return false;
    }

    private boolean shipSunken() {
        for(int[] shipField:wholeShip) {
            char fieldContent = boardCoordinates[shipField[0]][shipField[1]];
            if(fieldContent!=HIT) {
                return false;
            }
        }
        return true;
    }

    private List<int[]> getShipsCoordinates() {
        List<int[]> shipsCoordinates = new ArrayList<>();
        for(int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
                if(boardCoordinates[i][j] != '~') {
                    int[] shipInt = {i,j};

                    shipsCoordinates.add(shipInt);
                }
            }
        }
        return shipsCoordinates;
    }

    public void setName(String boardName) {
        this.boardName = boardName;
    }

    public String getName() {
        return boardName;
    }

    public boolean nameIsSet() {
        return !(boardName==null);
    }
}