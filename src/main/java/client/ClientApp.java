package client;

import board.InvalidCoordinateException;
import io.Intro;

public class ClientApp {

    public static void main(String[] args) throws InvalidCoordinateException {

        Intro intro = new Intro();
        intro.start();
    }
}
