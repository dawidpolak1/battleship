package client;

import api.request.Request;
import api.request.RequestCreator;
import api.response.Response;
import api.response.ResponseCreator;
import board.Board;
import board.BoardPrinter;
import board.InvalidCoordinateException;
import io.*;
import shared.ConnectionManager;

import java.io.IOException;
import java.net.Socket;
import java.util.Objects;

public class Client {
    private DataReader reader;
    private ConsolePrinter printer;
    private ConnectionManager connectionManager;
    private Board playerBoard;
    private Board serverBoard;

    public Client() {
        this.reader = new DataReader();
        this.printer = new ConsolePrinter();
    }

    public void run() throws InvalidCoordinateException {
        connectionManager = getConnection();
        if(!Objects.isNull(connectionManager)) {
            sendInvitation();
            runClientGameLoop();
        }
    }

    public void runClientGameLoop() throws InvalidCoordinateException {
        Response response = null;
        do {
            response = getResponse();
            if (serverIsFreeAndReady(response)) {
                prepareBoards();
                shootingPhase();
            } else if (serverIsBusy(response)) {
                printer.printLine("Server is busy at the moment, please try again later");
                break;
            }
        } while(!isTerminatingResponse(response));
    }

    private void prepareBoards() {
        printer.printLine("Game begins!");
        ClientBoardCreationIO clientBoardCreationIO = new ClientBoardCreationIO();
        playerBoard = clientBoardCreationIO.getBoardFromPlayer();
        serverBoard = new Board("Enemy board");
        BoardPrinter.printBoards(playerBoard,serverBoard);
    }

    private void shootingPhase() throws InvalidCoordinateException {
        printer.printLine("(Shooting phase)");
        printer.printLine("Type your shot:");
        Request shot = RequestCreator.getShot(reader.getString());
        sendRequest(shot);

        //TODO: create loop for shooting until all ships are sunked
    }

    private boolean serverIsBusy(Response response) {
        return response.equals(ResponseCreator.getGameInvitationResponse(false));
    }

    private boolean isTerminatingResponse(Response response) {
        return false;       //TODO: create logical conditions to terminate app
    }


    private boolean serverIsFreeAndReady(Response response) {
        return response.equals(ResponseCreator.getGameInvitationResponse(true));
    }

    private ConnectionManager getConnection() {
        try {
            Socket clientSocket = new Socket(ConnectionManager.IP, ConnectionManager.PORT);
            printer.printLine("Connected to server");
            return new ConnectionManager(clientSocket);
        } catch (IOException e) {
            printer.printLine("Wasn't able to connect to server");
            return null;
        }
    }

    private void sendInvitation() {
        Request invitation = RequestCreator.getGameInvitationRequest();
        sendRequest(invitation);
    }

    private void sendRequest(Request request) {
        String json = JsonManager.to(request);
        connectionManager.send(json);
    }

    private Response getResponse() throws InvalidCoordinateException {
        Response response = null;
        String responseJson = null;
        try {
            responseJson = connectionManager.readInputAndCheckConnection();
            response = JsonManager.from(responseJson, Response.class);
        } catch (IOException e) {
            System.out.println("Connection error occurred. Restarting Application.");
            connectionManager.stopConnection();
            run();
        }
        return response;
    }
}
