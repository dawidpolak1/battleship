package api.request;

import java.util.HashMap;
import java.util.Map;

public class Request {

    private final RequestType type;
    private Object body;

    public Request(RequestType requestType) {
        this.type = requestType;
    }

    public Request(RequestType requestType, String body) {
        this.type = requestType;
        this.body = body;
    }

    public Request(RequestType requestType, String row, int column) {
        this.type = requestType;
        Map<String, Object> map = new HashMap<>();
        map.put("row", row);
        map.put("column", column);
        this.body = map;
    }

    public Request(RequestType requestType, Map<String, String> shipsOnBoard) {
        this.type = requestType;
        this.body = shipsOnBoard;
    }

    public RequestType getType() {
        return type;
    }

    public Object getBody() {
        return body;
    }
}
