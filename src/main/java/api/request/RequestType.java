package api.request;

public enum RequestType {
        INVITE_TO_GAME, SHOT, SHOT_REQUEST, RESULT, SHOW_BOARD, UNKNOWN_REQUEST
    }