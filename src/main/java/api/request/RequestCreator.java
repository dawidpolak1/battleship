package api.request;

public class RequestCreator {

    public static Request getGameInvitationRequest() {
        return new Request(RequestType.INVITE_TO_GAME);
    }

    public static Request getShot(String shotCoordinate) {
        String row = Character.toString(shotCoordinate.charAt(0));
        int column = Character.getNumericValue(shotCoordinate.charAt(1));
        return new Request(RequestType.SHOT, row, column);
    }

    public static Request getShotRequest() {
        return new Request(RequestType.SHOT_REQUEST);
    }

    public static Request getHitConfirmation() {
        return new Request(RequestType.RESULT,"HIT");
    }

    public static Request getMissConfirmation() {
        return new Request(RequestType.RESULT,"MISS");
    }

    public static Request getSinkingConfirmation() {
        return new Request(RequestType.RESULT,"SINKING");
    }
}
