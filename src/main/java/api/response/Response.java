package api.response;

import api.request.RequestType;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Response {

    private final RequestType type;
    private final int status;
    private String message;
    private Object body;

    public Response(RequestType requestType, int status) {
        this.type = requestType;
        this.status = status;
    }

    public Response(RequestType requestType, int status, String message) {
        this.type = requestType;
        this.status = status;
        this.message = message;
    }

    public Response(RequestType requestType, int status, String body, String message) {
        this.type = requestType;
        this.status = status;
        this.body = body;
        this.message = message;
    }


    public Response(RequestType requestType, int status, String row, int column) {
        this.type = requestType;
        this.status = status;
        Map<String, Object> map = new HashMap<>();
        map.put("row", row);
        map.put("column", column);
        this.body = map;
    }


    public RequestType getType() {
        return type;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Object getBody() {
        return body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Response response = (Response) o;
        return status == response.status && type == response.type && Objects.equals(message, response.message) && Objects.equals(body, response.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, status, message, body);
    }
}
