package api.response;

import api.request.RequestType;

public class ResponseCreator {

    private static final String HIT = "HIT";
    private static final String MISS = "MISS";
    private static final String SINKING = "SINKING";

    public static Response getGameInvitationResponse(boolean isFree) {
        if (isFree) {
            return new Response(RequestType.INVITE_TO_GAME, Status.OK.getValue());
        } else {
            return new Response(RequestType.INVITE_TO_GAME, Status.SERVER_BUSY.getValue(), "Server is playing another game");
        }
    }

    public static Response getShotResponse(String result) {
        if (result.equalsIgnoreCase(HIT)) {
            return new Response(RequestType.SHOT, Status.OK.getValue(), HIT, null);
        } else if (result.equalsIgnoreCase(MISS)) {
            return new Response(RequestType.SHOT, Status.OK.getValue(), MISS, null);
        } else if (result.equalsIgnoreCase(SINKING)) {
            return new Response(RequestType.SHOT, Status.OK.getValue(), SINKING, null);
        } else {
            return new Response(RequestType.SHOT, Status.ILLEGAL_ARGUMENTS.getValue(), null, "The shot is not within the boundaries of the board");
        }
    }

    public static Response getShotRequestResponse(String shotCoordinate) {
        String row = Character.toString(shotCoordinate.charAt(0));
        int column = Character.getNumericValue(shotCoordinate.charAt(1));
        return new Response(RequestType.SHOT_REQUEST, Status.OK.getValue(), row, column);
    }

    public static Response getShotResultConfirmation() {
        return new Response(RequestType.RESULT, Status.OK.getValue());
    }

    public static Response getBoardConfirmation() {
        return new Response(RequestType.SHOW_BOARD, Status.OK.getValue());
    }

    public static Response getUnknownRequestResponse() {
        return new Response(RequestType.UNKNOWN_REQUEST, Status.BAD_REQUEST.getValue());
    }
}
