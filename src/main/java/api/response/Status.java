package api.response;

public enum Status {

        OK(0),
        SERVER_BUSY(1),
        ILLEGAL_ARGUMENTS(2),
        INTERNAL_ERROR(3),
        BAD_REQUEST(4);

        private final int value;

        Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }