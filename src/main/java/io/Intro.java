package io;

import board.InvalidCoordinateException;
import client.Client;
import server.Server;

import java.util.InputMismatchException;

public class Intro {
    private DataReader reader;
    private ConsolePrinter printer;

    private static final String LOGO = "  :::::   ::: :::::::::      :::     ::::::::::: ::::::::::: :::        ::::::::::                ::::::::  :::    ::: ::::::::::: :::::::::    :::::   ::: \n" +
            ":+:   :+:+:   :+:    :+:   :+: :+:       :+:         :+:     :+:        :+:         :+:     :+:  :+:    :+: :+:    :+:     :+:     :+:    :+: :+:   :+:+:   \n" +
            "              +:+    +:+  +:+   +:+      +:+         +:+     +:+        +:+           +:+ +:+    +:+        +:+    +:+     +:+     +:+    +:+               \n" +
            "              +#++:++#+  +#++:++#++:     +#+         +#+     +#+        +#++:++#   +#++:++#++:++ +#++:++#++ +#++:++#++     +#+     +#++:++#+                \n" +
            "              +#+    +#+ +#+     +#+     +#+         +#+     +#+        +#+           +#+ +#+           +#+ +#+    +#+     +#+     +#+                      \n" +
            "              #+#    #+# #+#     #+#     #+#         #+#     #+#        #+#         #+#     #+#  #+#    #+# #+#    #+#     #+#     #+#                      \n" +
            "              #########  ###     ###     ###         ###     ########## ##########                ########  ###    ### ########### ###                      \n";

    public Intro() {
        this.reader = new DataReader();
        this.printer = new ConsolePrinter();
    }

    public void start() throws InvalidCoordinateException {
        printMainMenu();
        GameType gameType = getGameType();
        switchGameType(gameType);
    }

    public void printMainMenu() {
        printer.printLine(LOGO);
        printer.printLine("Ahoy! Select mode ->");
        printer.printGameTypes();
    }

    private void switchGameType(GameType gameType) throws InvalidCoordinateException {
        switch (gameType) {
            case CONSOLE_CLIENT -> {
                Client client = new Client();
                client.run();
            }
            //case AI_CLIENT -> new AIClient(); //TODO: create AIClient module (low priority)
            case SERVER -> {
                Server server = new Server();
                server.run();
            }
            case EXIT_GAME -> exit();
            default -> printer.printLine("Unknown command");

        }
    }

    private GameType getGameType() {
        boolean optionOk = false;
        GameType gameType = null;
        while (!optionOk) {
            try {
                gameType = GameType.createFromInt(reader.getInt());
                optionOk = true;
            } catch (IllegalArgumentException | InputMismatchException e) {
                printer.printLine("Invalid option. Please try again.");     //TODO: sometimes error is printed twice, only after second invalid input, let's try to fix it.
            }
        }
        return gameType;
    }

    private void exit() {
        reader.closeInput();
        printer.printLine("Application terminated. Goodbye!");
    }

}
