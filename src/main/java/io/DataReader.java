package io;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DataReader {

    private final Scanner sc = new Scanner(System.in);

    public String getString() {
        return sc.nextLine();
    }

    public int getInt() throws InputMismatchException {
        if(sc.hasNextInt()) {
            return sc.nextInt();
        }
        else {
            sc.nextLine();
            throw new InputMismatchException();
        }
    }

    public void closeInput() {
        sc.close();
    }
}
