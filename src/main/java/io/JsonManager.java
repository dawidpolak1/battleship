package io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonManager {

    public static String to(Object object) {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(object);
    }

    public static <T> T from(String json, Class<T> classType) {
        return new Gson().fromJson(json, classType);
    }
}
