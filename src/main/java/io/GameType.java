package io;

public enum GameType {

    CONSOLE_CLIENT(0, " - Connect to server and play against AI"),
    AI_CLIENT(1, " - Connect to server and let AI play against AI"),
    SERVER(2, " - Start server and wait for a client"),
    EXIT_GAME(3, " - Stop application");

    private final int value;
    private final String description;

    GameType(int value, String description) {
        this.value = value;
        this.description = description;
    }

    @Override
    public String toString() {
        return value + " " + description;
    }

    static GameType createFromInt(int option) {
        try {
            return GameType.values()[option];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException("There is no option with id " + option);
        }
    }
}
