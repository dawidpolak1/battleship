package io;

import board.Board;
import board.BoardCreator;
import board.BoardGenerator;
import board.InvalidCoordinateException;

import java.util.InputMismatchException;

public class ClientBoardCreationIO {
    private DataReader reader;
    private ConsolePrinter printer;
    private Board board;

    public ClientBoardCreationIO() {
        this.reader = new DataReader();
        this.printer = new ConsolePrinter();
    }

    public Board getBoardFromPlayer() {
        viewCreateBoardMenu();
        return board;
    }

    public void viewCreateBoardMenu() {
        printer.printLine("Choose option:");
        printBoardCreationOptions();
        BoardCreationMode creationMode = getCreationMode();
        runCreationMode(creationMode);
    }

    void printBoardCreationOptions() {
        for (BoardCreationMode mode : BoardCreationMode.values()) {
            printer.printLine(mode.toString());
        }
    }

    private void runCreationMode(BoardCreationMode creationMode) {
        switch (creationMode) {
            case MANUAL -> {
                createBoard();
            }
            case AUTO -> {
                generateBoard();
            }
            case EXIT_GAME -> exit();
            default -> printer.printLine("Unknown command");

        }
    }

    private void createBoard() {
        BoardCreator boardCreator = new BoardCreator();
        String input = reader.getString();
        while(!boardCreator.isComplete()) {
            try {
                String shipCoordinate = getInput("Choose ship placement: ");
                boardCreator.placeShip(shipCoordinate);
            } catch (InvalidCoordinateException e) {
                printer.printLine("Invalid coordinate, please try again.");
            } finally {
                boardCreator.print();
            }
        }
        System.out.println("Board completed!");
        board = boardCreator.getBoard();
        setBoardName();
    }

    private String getInput(String question) {
        printer.printLine(question);
        String input = reader.getString();
        return input;
    }

    private void generateBoard() {
        printer.printLine("Random board generated.");
        BoardGenerator boardGenerator = new BoardGenerator();
        board = boardGenerator.getBoard();
        reader.getString();
        setBoardName();
        board.print();
    }

    private void setBoardName() {
        String boardName = getInput("Choose name for your board");
        board.setName(boardName);
    }

    private BoardCreationMode getCreationMode() {
        boolean optionOk = false;
        BoardCreationMode creationMode = null;
        while (!optionOk) {
            try {
                creationMode = BoardCreationMode.createFromInt(reader.getInt());
                optionOk = true;
            } catch (IllegalArgumentException | InputMismatchException e) {
                printer.printLine("Invalid option. Please try again.");     //TODO: sometimes error is printed twice, only after second invalid input, let's try to fix it.
            }
        }
        return creationMode;
    }


    private void exit() {
        reader.closeInput();
        printer.printLine("Quitting...");       //TODO: find a way to get to previous menu
    }

    public Board getBoard() {
        return board;
    }
}
