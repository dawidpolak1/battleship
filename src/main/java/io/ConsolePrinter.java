package io;

public class ConsolePrinter {

     void printGameTypes() {
        for (GameType mode : GameType.values()) {
            printLine(mode.toString());
        }
    }

    public void printLine(String text) {
        System.out.println(text);
    }
}
