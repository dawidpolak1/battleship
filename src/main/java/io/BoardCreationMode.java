package io;

public enum BoardCreationMode {

    MANUAL(0, " - Create board by putting ships manually on board"),
    AUTO(1, " - Get randomly generated board"),
    EXIT_GAME(2, " - Stop application");

    private final int value;
    private final String description;

    BoardCreationMode(int value, String description) {
        this.value = value;
        this.description = description;
    }

    @Override
    public String toString() {
        return value + " " + description;
    }

    static BoardCreationMode createFromInt(int option) {
        try {
            return BoardCreationMode.values()[option];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException("There is no option with id " + option);
        }
    }
}
