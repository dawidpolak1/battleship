package server.service;

import api.request.Request;
import api.request.RequestType;
import api.response.Response;
import api.response.ResponseCreator;
import board.Board;
import board.InvalidCoordinateException;

public class GameLogicEngine {
    private Board playerBoard;
    private Board serverBoard;
    private boolean terminateGame;
    private boolean isFree = true;

    public GameLogicEngine(Board playerBoard, Board serverBoard) {
        this.playerBoard = playerBoard;
        this.serverBoard = serverBoard;
    }

    public Response process(Request request) throws InvalidCoordinateException {
        Response response = null;
        if (request.getType().equals(RequestType.INVITE_TO_GAME)) {
            response = ResponseCreator.getGameInvitationResponse(isFree);
            isFree = false;
        } else if (request.getType().equals(RequestType.SHOT)) {
            String shot = extractCoordinate(request);
            String result = serverBoard.shoot(shot);
            response = ResponseCreator.getShotResponse(result);

        }
        return response;
    }

    private String extractCoordinate(Request request) {
        String column = null;
        String row = null;
        String body = request.getBody().toString();
        String[] bodyParts = body.replaceAll("[{} (.0)]", "").split("[,]");
        for (String bodyPart : bodyParts) {
            if (bodyPart.contains("column=")) {
                column = bodyPart.split("=")[1];
            } else if (bodyPart.contains("row=")) {
                row = bodyPart.split("=")[1];
            }
        }
        if (row != null && column != null) {
            return row.toUpperCase() + column.toUpperCase();
        }
        return null;
    }

    public boolean gameShouldBeTerminated() {
        return terminateGame;
    }
}
