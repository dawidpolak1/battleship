package server;

import api.request.Request;
import api.response.Response;
import board.Board;
import board.BoardGenerator;
import board.InvalidCoordinateException;
import io.ConsolePrinter;
import io.DataReader;
import io.Intro;
import io.JsonManager;
import server.service.GameLogicEngine;
import shared.ConnectionManager;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    ConnectionManager connectionManager;
    GameLogicEngine gameLogicEngine;
    private DataReader reader;
    private ConsolePrinter printer;

    boolean isFree=true;

    public Server() {
        this.reader = new DataReader();
        this.printer = new ConsolePrinter();
    }

    public void run() {
        printer.printLine("Server is up and running. Waiting for connection."); //TODO: redo this command using printer, find a way to get out of this mode  ("Press "Q" to terminate server.")
        connectionManager = getConnection();
        isFree=false;
        prepareBoards();
        hostGameLoop();
    }

    private void hostGameLoop() {
        Request request;
        do {
            try {
                request = getRequest();
                Response response = gameLogicEngine.process(request);
                sendResponse(response);
            } catch(IOException e) {
                System.out.println("Connection error occurred. Restarting Server.");
                connectionManager.stopConnection();
                isFree=true;
                run();
                break;
            } catch(NullPointerException e) {
                isFree=true;
                break;
            } catch (InvalidCoordinateException e) {
                e.printStackTrace();
            }
        } while (!gameLogicEngine.gameShouldBeTerminated());//TODO: set a break loop command to quit server mode
    }

    private void prepareBoards() {
        Board playerBoard = new Board();
        BoardGenerator boardGenerator = new BoardGenerator();
        Board serverBoard = boardGenerator.getBoard();
        gameLogicEngine = new GameLogicEngine(playerBoard, serverBoard);
    }

    private ConnectionManager getConnection() {
        ConnectionManager connectionManager = null;
        try (ServerSocket serverSocket = new ServerSocket(ConnectionManager.PORT)) {
            Socket clientSocket = serverSocket.accept();
            connectionManager = new ConnectionManager(clientSocket);
            System.out.println("Client has connected.");
            return connectionManager;
        } catch (BindException e) {
            System.out.println("Address already in use. Restarting Battleships.\n");
            Intro intro = new Intro();
            intro.start();
        } catch (IOException e) {
        }
        finally {
            return connectionManager;
        }
    }

    private Request getRequest() throws IOException {
        String json = connectionManager.readInputAndCheckConnection();
        return JsonManager.from(json, Request.class);
    }

    private void sendResponse(Response response) {
        String responseJson = JsonManager.to(response);
        connectionManager.send(responseJson);
    }
}