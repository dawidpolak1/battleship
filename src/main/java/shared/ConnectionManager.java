package shared;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ConnectionManager {

    public static final String IP = "127.0.0.1";
    public static final int PORT = 6665;
    private static final String NOT_CONNECTED = "Not connected";
    private final Socket socket;
    private final PrintWriter out;
    private final BufferedReader in;

    public ConnectionManager(Socket socket) throws IOException {
        this.socket = socket;
        out = new PrintWriter(socket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public void send(String json) {
        out.println(json);
    }

    public String readInputAndCheckConnection() throws IOException {
        try {
            String json = in.readLine();
            if (json == null) {
                return NOT_CONNECTED;
            }
            return json;
        } catch (IOException e) {
            stopConnection();
            throw e;
        }
    }

    public void stopConnection() {
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
