package board;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static board.CoordinatesTools.*;

class CoordinatesToolsTest {

    @Test
    void shouldTransformSingleCoordinate() {
        int[] transformedCoordinates=null;
        try{
            transformedCoordinates = transformCoordinates("A1");
        } catch (InvalidCoordinateException exc) {

        }

        Assertions.assertEquals(0,transformedCoordinates[0]);
        Assertions.assertEquals(0,transformedCoordinates[1]);
        Assertions.assertEquals(2,transformedCoordinates.length);
    }

    @Test
    void shouldTransformTwoPointCoordinate() {
        int[] transformedCoordinates=null;
        try{
            transformedCoordinates = transformCoordinates("A1-A2");
        } catch (InvalidCoordinateException exc) {

        }

        Assertions.assertEquals(0,transformedCoordinates[0]);
        Assertions.assertEquals(0,transformedCoordinates[1]);
        Assertions.assertEquals(0,transformedCoordinates[2]);
        Assertions.assertEquals(1,transformedCoordinates[3]);
        Assertions.assertEquals(4,transformedCoordinates.length);
    }

    @Test
    void shouldTransformThreePointCoordinate() {
        int[] transformedCoordinates=null;
        try{
            transformedCoordinates = transformCoordinates("A1-A3");
        } catch (InvalidCoordinateException exc) {

        }

        Assertions.assertEquals(0,transformedCoordinates[0]);
        Assertions.assertEquals(0,transformedCoordinates[1]);
        Assertions.assertEquals(0,transformedCoordinates[2]);
        Assertions.assertEquals(1,transformedCoordinates[3]);
        Assertions.assertEquals(0,transformedCoordinates[4]);
        Assertions.assertEquals(2,transformedCoordinates[5]);
        Assertions.assertEquals(6,transformedCoordinates.length);
    }

    @Test
    void ShouldTransformFourPointCoordinate() {
        int[] transformedCoordinates=null;
        try{
            transformedCoordinates = transformCoordinates("A1-A4");
        } catch (InvalidCoordinateException exc) {

        }

        Assertions.assertEquals(0,transformedCoordinates[0]);
        Assertions.assertEquals(0,transformedCoordinates[1]);
        Assertions.assertEquals(0,transformedCoordinates[2]);
        Assertions.assertEquals(1,transformedCoordinates[3]);
        Assertions.assertEquals(0,transformedCoordinates[4]);
        Assertions.assertEquals(2,transformedCoordinates[5]);
        Assertions.assertEquals(0,transformedCoordinates[6]);
        Assertions.assertEquals(3,transformedCoordinates[7]);
        Assertions.assertEquals(8,transformedCoordinates.length);
    }


    @Test
    void ShouldThrowExceptionFromInvalidCoordinate() {
        Assertions.assertThrows(InvalidCoordinateException.class, () -> transformCoordinates("Zgaga"));
    }


    @Test
    void ShouldThrowExceptionFromAlmostValidCoordinate() {
        Assertions.assertThrows(InvalidCoordinateException.class, () -> transformCoordinates("GZ"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> transformCoordinates("K9"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> transformCoordinates("A11"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> transformCoordinates("1A"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> transformCoordinates("a1"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> transformCoordinates("A1-C3"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> transformCoordinates("F9-F11"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> transformCoordinates("---F"));
    }

    @Test
    void shouldReturnTrueForValidCoordinateFormat() {
        Assertions.assertTrue(isValidCoordinate("A1"));
        Assertions.assertTrue(isValidCoordinate("J1"));
        Assertions.assertTrue(isValidCoordinate("A10"));
        Assertions.assertTrue(isValidCoordinate("J10"));
        Assertions.assertTrue(isValidCoordinate("E6"));
        Assertions.assertTrue(isValidCoordinate("A1-A10"));
        Assertions.assertTrue(isValidCoordinate("A1-D1"));
        Assertions.assertTrue(isValidCoordinate("D1-A1"));
        Assertions.assertTrue(isValidCoordinate("J8-J9"));
    }

    @Test
    void shouldReturnFalseForValidCoordinateFormat() {
        Assertions.assertFalse(isValidCoordinate("A11"));
        Assertions.assertFalse(isValidCoordinate("K10"));
        Assertions.assertFalse(isValidCoordinate("9A"));
        Assertions.assertFalse(isValidCoordinate("A8-A12"));
        Assertions.assertFalse(isValidCoordinate("I9-L91"));
        Assertions.assertFalse(isValidCoordinate("I-D"));
        Assertions.assertFalse(isValidCoordinate("89-22"));
        Assertions.assertFalse(isValidCoordinate("Plansza"));
        Assertions.assertFalse(isValidCoordinate("A1-C3"));
    }

    @Test
    void shouldReturnCorrectShipSize() {
        try{
            Assertions.assertEquals(1,getShipSize("A1"));
            Assertions.assertEquals(2,getShipSize("G10-G9"));
            Assertions.assertEquals(3,getShipSize("A1-A3"));
            Assertions.assertEquals(6,getShipSize("F5-F10"));
            Assertions.assertEquals(4,getShipSize("A1-D1"));
        } catch (InvalidCoordinateException exc ) {

        }
    }

    @Test
    void shouldThrowExceptionOverIncorrectShipSize() {
        Assertions.assertThrows(InvalidCoordinateException.class, () -> getShipSize("GZ"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> getShipSize("25Z"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> getShipSize("K1-K12"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> getShipSize("--_-"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> getShipSize("Sanki"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> getShipSize("-1A"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> getShipSize("-1A-A1"));
        Assertions.assertThrows(InvalidCoordinateException.class, () -> getShipSize("A1-C3"));
    }

}