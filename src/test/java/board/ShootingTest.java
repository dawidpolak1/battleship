package board;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ShootingTest {

    Board board;
    char[][] coordinates;

    @BeforeEach
    void init() {
        coordinates = new char[][]{
                "O~~~~O~~OO".toCharArray(),
                "O~~~~~~~~~".toCharArray(),
                "O~~O~~~~~~".toCharArray(),
                "~~~O~~~~~~".toCharArray(),
                "~~~~~O~O~O".toCharArray(),
                "~~~~~~~O~~".toCharArray(),
                "~~~~~~~O~~".toCharArray(),
                "~OOOO~~~~~".toCharArray(),
                "~~~~~~O~~O".toCharArray(),
                "~~~~~~O~~~".toCharArray()};
        board = new Board("Enemy", coordinates);
    }

    @Test
    void areAllShipsShoot() throws InvalidCoordinateException {

        board.shoot("C8");
        board.shoot("E10");
        board.shoot("C8");
        board.shoot("A1");
        board.shoot("C1");
        board.shoot("H4");
        board.shoot("B1");

        assertFalse(board.allShipsSunken(), "Are all ships shoot?");

        board.shoot("A6");
        board.shoot("A9");
        board.shoot("A10");
        board.shoot("C4");
        board.shoot("D4");
        board.shoot("E6");
        board.shoot("E8");
        board.shoot("F8");
        board.shoot("G8");
        board.shoot("H2");
        board.shoot("H3");
        board.shoot("H5");
        board.shoot("I7");
        board.shoot("I10");
        board.shoot("J7");

        assertTrue(board.allShipsSunken(), "And now, are all ships shoot?");
    }
}
